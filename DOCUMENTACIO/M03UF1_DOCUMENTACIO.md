# M03 UF1: PROJECTE WORDLE

## PROJECTE
---
* El projecte es una aplicació de consola que imita el joc "Wordle". En aquest has d'adivinar una paraula secreta abans de X intents mentres el joc et dona pistes de quines lletres encertes.

![Banner de Wordle](./wordle.png)

## IMPLEMENTACIONS
---
* El joc implementa una petita pantalla de selecció on l'usuari pot escollir si jugar o no, aquesta pantalla surt al principi del joc per explicarte les regles i tornaràs cada vegada que acabis una partida per decidir si vols seguir jugant o si vols tancar el programa.

![Screenshot de la consola](PROJECTE01.png)

* Durant el joc, el programa escolleix una paraula aleatoria d'una llista de 20 i comença a demanar intents. Es fa càrrec de que l'usuari introdueixi paraules del tamany correcte. No n'hi ha cap problema amb caracters especials ni números, però obviament no es troben dins de la llista de paraules.

![Screenshot del codi](PROJECTE02b.png)

![Screenshot de la consola](PROJECTE02a.png)

* El programa llegeix l'input de l'usuari i pasa lletra per lletra per comprovar si està en el lloc correcte (i marca la lletra en **verd**), i sino com a mínim si està en la paraula secreta (i marca la lletra en **groc**). 

![Screenshot de la consola](PROJECTE03b.png)

![Screenshot de la consola](PROJECTE03a.png)

* Finalment comprova si has fallat en alguna lletra o no, en cas de que no adivinis la paraula després de 6 intents, perds i et mostra quina era la paraula.

![Screenshot de la consola](PROJECTE04b.png)

![Screenshot de la consola](PROJECTE04a.png)

### EXTRAS

* Addicionalment al que s'ha demanat, he implementat la possibilitat d'introduir paraules de diferents llargades al vector de possibles paraules secretes (el programa sempre t'avisarà de la llargada de la paraula secreta)

* He implementat un sistema d'intents que dona la possibilitat de perdre el joc.

* El programa es multipartida

### NOTES

* De moment no n'hi ha plans per restringir l'input del jugador exclusivament a paraules del diccionari (com ho fa el wordle original)

* Encara que suporta paraules de varies lletres, si s'escriu per exemple 5 vegades 'A' en una paraula amb dues 'A' el programa marcarà les 5 'A' (tant les verdes como grogues). Idealment hauria de marcar només tantes 'A' com hi hagi a la paraula secreta, donant prioritat a les que estan el lloc correcte (verdes). 

* Encara que es multipartida, el programa no guarda informació de partides anteriors.