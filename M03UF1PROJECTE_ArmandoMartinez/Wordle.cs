﻿/*
 * AUTHOR: Armando Martínez
 * DATE:
 * DESCRIPTION: Implementació de Wordle per terminal
 */

using System;

namespace M03UF1PROJECTE_ArmandoMartinez
{
    class Wordle
    {
        public void Game()
        {
            int tries = 6;
            bool correct;
            Console.WriteLine("\nEsbrina la paraula! Tens 6 intents!");
            string[] words = { "HOLA", "JAZZ", "MATRIU", "VECTOR", "INTEGER", "EXEMPLE", "CARCASA" }; 
            // TAMBÉ FUNCIONA AMB PARAULES DE DIFERENTS LONGITUTS, EN AQUEST CAS ES DEMANAVEN DE 5 LLETRES
            Random rnd = new Random();
            string selected_word = words[rnd.Next(0, words.Length)];
            do
            {
                Console.WriteLine($"La paraula es de {selected_word.Length} lletres");
                string user_word;
                do
                {
                    user_word = Console.ReadLine();
                    if (user_word.Length != selected_word.Length) { Console.WriteLine($"Ha de ser una paraula de {selected_word.Length} lletres!"); }
                } while (user_word.Length != selected_word.Length);
                user_word = user_word.ToUpper();
                correct = true;
                for (int i = 0; i < selected_word.Length; i++)
                {
                    if (user_word[i] == selected_word[i])
                    {
                        Console.BackgroundColor = ConsoleColor.Green;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.Write($" {user_word[i]} ");
                    }
                    else
                    {
                        correct = false;
                        bool inSelected = false;
                        for (int j = 0; j < selected_word.Length; j++)
                        {
                            if (user_word[i] == selected_word[j]) { inSelected = true; }
                        }
                        if (inSelected) 
                        {
                            Console.BackgroundColor = ConsoleColor.Yellow;
                            Console.ForegroundColor = ConsoleColor.Black;
                            Console.Write($" {user_word[i]} ");
                        }
                        else { Console.Write($" {user_word[i]} "); }
                    }
                    Console.ResetColor();
                }
                Console.WriteLine();
                if (!correct) { Console.WriteLine("Resposta incorrecta! Intenta-ho de nou!"); }
                tries -= 1;
            } while (!correct && tries > 0);
            if (correct) { Console.WriteLine("Bon treball! Has encertat!"); }
        }

        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Black;
            Console.BackgroundColor = ConsoleColor.White;
            Console.WriteLine("B E N V I N G U T   A   W O R D L E !");
            Console.ResetColor();
            Console.WriteLine("\nEn aquest joc has d'esbrinar una paraula secreta. Tindràs 6 intents i amb cada intent rebràs pistes sobre quant t'hi has apropat");
            Console.ForegroundColor = ConsoleColor.Black;
            Console.BackgroundColor = ConsoleColor.Yellow;
            Console.WriteLine("El color groc significa que la lletra es troba a la paraula però no en el lloc correcte.");
            Console.BackgroundColor = ConsoleColor.Green;
            Console.WriteLine("El color verd significa que la lletra es troba exactament en el lloc correcte de la paraula.");
            Console.ResetColor();
            bool correct_option = false;
            bool play = false;
            do
            {
                Console.Write("VOLS JUGAR? [S]í / [N]o  ");
                string option = Console.ReadLine();
                switch (option)
                {
                    case "S":
                    case "s":
                        correct_option = true;
                        play = true;
                        break;
                    case "N":
                    case "n":
                        correct_option = true;
                        play = false;
                        break;
                    default:
                        Console.WriteLine("OPCIÓ INCORRECTA! INTENTA-HO DE NOU!");
                        break;
                }
            } while (!correct_option);
            var wordle = new Wordle();
            while (play)
            {
                Console.Clear();
                wordle.Game();
                do
                {
                    Console.Write("VOLS TORNAR A JUGAR? [S]í / [N]o  ");
                    string option = Console.ReadLine();
                    switch (option)
                    {
                        case "S":
                        case "s":
                            correct_option = true;
                            play = true;
                            break;
                        case "N":
                        case "n":
                            correct_option = true;
                            play = false;
                            break;
                        default:
                            Console.WriteLine("OPCIÓ INCORRECTA! INTENTA-HO DE NOU!");
                            break;
                    }
                } while (!correct_option);
            }
            Console.WriteLine("Fins la próxima...");
        }
    }
}
